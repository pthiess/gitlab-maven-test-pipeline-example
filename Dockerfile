FROM maven:3.9.1-eclipse-temurin-17
COPY pom.xml /home/app/pom.xml
COPY src /home/app/src
CMD ["mvn", "test", "-f", "/home/app/pom.xml"]